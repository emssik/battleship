package pl.uncleglass.battleship;

public class BattleshipApp {
    public static void main(String[] args) {
        var starter = new Starter();
        starter.start();
    }
}
