package pl.uncleglass.battleship.dtos;

import java.util.ArrayList;
import java.util.List;

public class BoardDto {
    private final String four;
    private final List<String> three;
    private final List<String> two;
    private final List<String> one;

    public BoardDto(
            String four,
            List<String> three,
            List<String> two,
            List<String> one) {
        this.four = four;
        this.three = three;
        this.two = two;
        this.one = one;
    }

    public List<String> getAsList() {
        List<String> list = new ArrayList<>(getAllCoordinates(four));
        for (String ship : three) {
            list.addAll(getAllCoordinates(ship));
        }
        for (String ship : two) {
            list.addAll(getAllCoordinates(ship));
        }
        list.addAll(one);
        return list;
    }

    private List<String> getAllCoordinates(String ship) {
        List<String> coordinates = new ArrayList<>();
        String[] split = ship.split("-");

        String head = split[0];
        String tail = split[1];
        if (isHorizontal(head, tail)) {
            int headColumn = Integer.parseInt(head.substring(1));
            int tailColumn = Integer.parseInt(tail.substring(1));
            String headRow = head.substring(0, 1);
            int stoper = tailColumn - headColumn;
            for (int i = 0; i <= stoper; i++) {
                coordinates.add(headRow + headColumn);
                headColumn++;
            }
        } else {
            char headRow = head.toCharArray()[0];
            char tailRow = tail.toCharArray()[0];
            String headColumn = head.substring(1);
            int stoper = tailRow - headRow;
            for (int i = 0; i <= stoper; i++) {
                coordinates.add(Character.toString(headRow) + headColumn);
                headRow++;
            }
        }
        return coordinates;
    }

    private boolean isHorizontal(String head, String tail) {
        char[] headChars = head.toCharArray();
        char[] tailChars = tail.toCharArray();
        return headChars[0] - tailChars[0] == 0;
    }
}
