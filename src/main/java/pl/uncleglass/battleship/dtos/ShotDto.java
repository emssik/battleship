package pl.uncleglass.battleship.dtos;

public class ShotDto {
    private final char row;
    private final int column;

    private ShotDto(char row, int column) {
        this.row = Character.toUpperCase(row);
        this.column = column;
    }

    public static ShotDto of(char row, int column) {
        return new ShotDto(row, column);
    }

    public char getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public boolean verify() {
        return row >= 'A' && row <= 'J' &&
                column >= 1 && column <= 10;
    }

    @Override
    public String toString() {
        return Character.toString(row) +
                column;
    }
}
