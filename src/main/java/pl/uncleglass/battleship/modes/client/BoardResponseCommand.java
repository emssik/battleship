package pl.uncleglass.battleship.modes.client;

import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.response.Response;

class BoardResponseCommand extends Command {
    public BoardResponseCommand(Response response) {
        super(response);
    }

    @Override
    public Request execute(Game game) {
        game.stop();
        return Request.unknown();
    }
}
