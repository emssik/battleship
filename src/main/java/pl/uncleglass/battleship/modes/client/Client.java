package pl.uncleglass.battleship.modes.client;

import pl.uncleglass.battleship.UserInterface;
import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.modes.Mode;
import pl.uncleglass.battleship.modes.shared.Connection;
import pl.uncleglass.battleship.modes.shared.JsonConverter;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.request.RequestType;
import pl.uncleglass.battleship.modes.shared.response.Response;

import java.io.IOException;
import java.net.Socket;

public class Client implements Mode {
    private Connection server;
    private final Game game;

    public Client(Game game) {
        this.game = game;
    }

    @Override
    public boolean run() {
        server = establishConnection();
        if (server == null) {
            return true;
        }

        var invitation = Request.gameInvitation();
        sendRequest(invitation);
        while (server.isConnected() && game.isContinued()) {
            UserInterface.print("======================ROUND======================");
            var response = getResponse();
            var command = CommandFactory.get(response);
            var request = command.execute(game);
            sendRequest(request);
        }

        server.close();
        return true;
    }

    private Connection establishConnection() {
        UserInterface.print("Enter the IP of the server you want to connect to:");
        String ip = UserInterface.read();
        UserInterface.print("Enter the PORT number:");
        String port = UserInterface.read();
        try {
            var socket = new Socket(ip, Integer.parseInt(port));
            return new Connection(socket);
        } catch (IOException e) {
            UserInterface.print("Failed to establish connection");
        }
        return null;
    }

    private Response getResponse() {
        var read = server.readAndCheckIfConnected();
        return JsonConverter.from(read, Response.class);
    }

    private void sendRequest(Request request) {
        if (request.getType() == RequestType.UNKNOWN) {
            return;
        }
        var to = JsonConverter.to(request);
        server.write(to);
    }
}
