package pl.uncleglass.battleship.modes.client;

import pl.uncleglass.battleship.modes.shared.response.Response;

class CommandFactory {
    private CommandFactory() {
    }

    public static Command get(Response response) {

        return switch (response.getType()) {
            case GAME_INVITATION -> new GameInvitationResponseCommand(response);
            case SHOT -> new ShotResponseCommand(response);
            case SHOT_REQUEST -> new ShotRequestResponseCommand(response);
            case RESULT -> new ResultResponseCommand(response);
            case BOARD -> new BoardResponseCommand(response);
            case UNKNOWN -> new UnknownCommand(response);
        };
    }
}
