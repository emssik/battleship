package pl.uncleglass.battleship.modes.client;

import pl.uncleglass.battleship.UserInterface;
import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.response.Response;

class ResultResponseCommand extends Command {
    public ResultResponseCommand(Response response) {
        super(response);
    }

    @Override
    public Request execute(Game game) {
        if (game.whetherOpponentWon()) {
            UserInterface.print("Game over");
            UserInterface.print("You lost the game");
            return Request.board(game.getBoardFilling());
        }
        if (game.isOpponentLastShotAHit()) {
            UserInterface.print("Opponent last shot was successful\nShot Request was sent");
            return Request.shotRequest();
        } else {
            UserInterface.print("Shooting");
            var boards = game.getBoardsPrint();
            UserInterface.print(boards);
            var shot = game.getShot();
            UserInterface.print("My shot: " + shot);
            return Request.shot(shot);
        }
    }
}
