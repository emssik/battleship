package pl.uncleglass.battleship.modes.error;

import pl.uncleglass.battleship.UserInterface;
import pl.uncleglass.battleship.modes.Mode;

public class ErrorMode implements Mode {
    @Override
    public boolean run() {
        UserInterface.print("ERROR: Unknown command.");
        return true;
    }
}
