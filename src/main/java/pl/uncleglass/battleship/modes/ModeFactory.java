package pl.uncleglass.battleship.modes;

import pl.uncleglass.battleship.game.GameFactory;
import pl.uncleglass.battleship.modes.client.AIClient;
import pl.uncleglass.battleship.modes.client.Client;
import pl.uncleglass.battleship.modes.error.ErrorMode;
import pl.uncleglass.battleship.modes.exit.ExitMode;
import pl.uncleglass.battleship.modes.server.SerwerStarter;

public class ModeFactory {
    private ModeFactory() {
    }

    public static Mode get(String choice) {
        return switch (choice) {
            case "1" -> new Client(GameFactory.manualGame());
            case "2" -> new AIClient(GameFactory.aiGame());
            case "3" -> new SerwerStarter();
            case "4" -> new ExitMode();
            default -> new ErrorMode();
        };
    }
}
