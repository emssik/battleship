package pl.uncleglass.battleship.modes.server;

import org.sqlite.SQLiteDataSource;
import pl.uncleglass.battleship.dtos.ShotDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

class DataManager {
    private static final String URL = "jdbc:sqlite:db.s3db";

    public DataManager() {
        SQLiteDataSource dataSource = new SQLiteDataSource();
        dataSource.setUrl(URL);
        String sql = """
                CREATE TABLE IF NOT EXISTS games
                (
                    id       INTEGER PRIMARY KEY,
                    opponent text,
                    date     text
                );
                """;
        String sql2 = """
                CREATE  TABLE IF NOT EXISTS shots
                (
                    id       INTEGER PRIMARY KEY,
                    shot text,
                    result text,
                    who text,
                    game_id INTEGER NOT NULL,
                    FOREIGN KEY (game_id) REFERENCES games (id)
                );
                """;
        String sql3 = """
                CREATE  TABLE IF NOT EXISTS boards
                (
                    id       INTEGER PRIMARY KEY,
                    board text,
                    who text,
                    game_id INTEGER NOT NULL,
                    FOREIGN KEY (game_id) REFERENCES games (id)
                );
                """;
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute(sql);
            statement.execute(sql2);
            statement.execute(sql3);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public long saveGame(String clientName) {
        SQLiteDataSource dataSource = new SQLiteDataSource();
        dataSource.setUrl(URL);
        String sql = "INSERT INTO games(opponent, date) VALUES(?,?)";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            LocalDateTime now = LocalDateTime.now();
            DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
            statement.setString(1, clientName);
            statement.setString(2, now.format(format));
            statement.executeUpdate();
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getLong(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void saveServerShot(ShotDto actualShot, String result, long gameId) {
        saveShot(actualShot, result, "SERVER", gameId);
    }

    public void saveClientShot(ShotDto shot, String result, long gameId) {
        saveShot(shot, result, "CLIENT", gameId);
    }

    private void saveShot(ShotDto shot, String result, String who, long gameId) {
        SQLiteDataSource dataSource = new SQLiteDataSource();
        dataSource.setUrl(URL);
        String sql = "INSERT INTO shots(shot, result, who, game_id) VALUES(?,?,?,?)";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, shot.toString());
            statement.setString(2, result);
            statement.setString(3, who);
            statement.setLong(4, gameId);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveServerBoard(String board, long gameId) {
        saveBoard(board, "SERVER", gameId);
    }

    public void saveClientBoard(String board, long gameId) {
        saveBoard(board, "CLIENT", gameId);
    }

    private void saveBoard(String board, String who, long gameId) {
        SQLiteDataSource dataSource = new SQLiteDataSource();
        dataSource.setUrl(URL);
        String sql = "INSERT INTO boards(board,who, game_id) VALUES(?,?,?)";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, board);
            statement.setString(2, who);
            statement.setLong(3, gameId);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<GameInfo> getGamesInfo() {
        SQLiteDataSource dataSource = new SQLiteDataSource();
        dataSource.setUrl(URL);
        String sql = "SELECT * FROM games";
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            List<GameInfo> gameInfos = new ArrayList<>();
            while (resultSet.next()) {
                long id = resultSet.getLong("id");
                String opponentName = resultSet.getString("opponent");
                String date = resultSet.getString("date");
                gameInfos.add(new GameInfo(id, opponentName, date));
            }
            return gameInfos;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public String getServerBoard(long gameId) {
        return getBoard(gameId, "SERVER");
    }

    public String getClientBoard(long gameId) {
        return getBoard(gameId, "CLIENT");
    }

    private String getBoard(long gameId, String who) {
        SQLiteDataSource dataSource = new SQLiteDataSource();
        dataSource.setUrl(URL);
        String sql = "SELECT board FROM boards WHERE game_id = ? AND who = ?";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, gameId);
            statement.setString(2, who);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getString("board");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<CourseOfTheGame.Shot> getShots(long gameId) {
        List<CourseOfTheGame.Shot> shots = new ArrayList<>();
        SQLiteDataSource dataSource = new SQLiteDataSource();
        dataSource.setUrl(URL);
        String sql = "SELECT shot, result, who FROM shots WHERE game_id = ? ORDER BY id";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, gameId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                shots.add(new CourseOfTheGame.Shot(
                        resultSet.getString("shot"),
                        resultSet.getString("result"),
                        resultSet.getString("who")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return shots;
    }
}
