package pl.uncleglass.battleship.modes.server;

import pl.uncleglass.battleship.modes.Mode;
import pl.uncleglass.battleship.modes.error.ErrorMode;
import pl.uncleglass.battleship.modes.exit.BackMode;
import pl.uncleglass.battleship.modes.exit.ExitMode;

class ServerModeFactory {
    private ServerModeFactory() {
    }

    public static Mode get(String choice) {
        return switch (choice) {
            case "1" -> new Serwer();
            case "2" -> new BrowseMode();
            case "3" -> new BackMode();
            case "4" -> new ExitMode();
            default -> new ErrorMode();
        };
    }
}
