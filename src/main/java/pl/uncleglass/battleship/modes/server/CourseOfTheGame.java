package pl.uncleglass.battleship.modes.server;

import pl.uncleglass.battleship.UserInterface;
import pl.uncleglass.battleship.dtos.BoardDto;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

class CourseOfTheGame {
    private static final String HEADER = """
                               GAME START
            ===============================================
            """;
    private static final String ROUND_HEADER = """
            ===============================================
            Shoots: {0}
            Shot: {1}
            Result: {2}
            """;
    private static final String SERVER_HEADER = """
              Server board            Server shots
              1 2 3 4 5 6 7 8 9 10    1 2 3 4 5 6 7 8 9 10
            """;
    private static final String CLIENT_HEADER = """
              Client board            Client shots
              1 2 3 4 5 6 7 8 9 10    1 2 3 4 5 6 7 8 9 10
            """;
    private static final String FOOTER = """
            
            ===============================================
                               GAME OVER
            {0} won the game
            ===============================================
            """;

    private BoardDto serverShipsOnTheBoard;
    private BoardDto clientShipsOnTheBoard;
    private List<Shot> shots;

    static class Shot {
        private final String value;
        private final String result;
        private final String who;

        Shot(String value, String result, String who) {
            this.value = value;
            this.result = result;
            this.who = who;
        }

    }

    private static class Coordinate {
        private final int row;
        private final int column;

        private Coordinate(int row, int column) {
            this.row = row;
            this.column = column;
        }

        public static Coordinate from(String s) {
            char[] chars = s.toCharArray();
            var r = Character.toUpperCase(chars[0]);
            var row = r - 65;
            var col = Character.getNumericValue(chars[1]) - 1;
            if (chars.length == 3) {
                var c = s.substring(1);
                return new Coordinate(row, Integer.parseInt(c) - 1);
            }
            return new Coordinate(row, col);
        }
    }

    public void setServerShipsOnTheBoard(BoardDto serverShipsOnTheBoard) {
        this.serverShipsOnTheBoard = serverShipsOnTheBoard;
    }

    public void setClientShipsOnTheBoard(BoardDto clientShipsOnTheBoard) {
        this.clientShipsOnTheBoard = clientShipsOnTheBoard;
    }

    public void setShots(List<Shot> shots) {
        this.shots = shots;
    }

    public void display(long speed) {
        char[][] serverBoard = fulfilBoard(getEmptyBoard(), serverShipsOnTheBoard);
        char[][] serverShots = getEmptyBoard();
        char[][] clientBoard = fulfilBoard(getEmptyBoard(), clientShipsOnTheBoard);
        char[][] clientShots = getEmptyBoard();

        UserInterface.print(HEADER);
        String frame = generateFrame(serverBoard, serverShots, clientBoard, clientShots);
        UserInterface.print(frame);
        for (var shot : shots) {
            try {
                Thread.sleep(speed);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            updateBoards(shot, serverBoard, serverShots, clientBoard, clientShots);
            UserInterface.print(MessageFormat.format(ROUND_HEADER, shot.who, shot.value, shot.result));
            UserInterface.print(generateFrame(serverBoard, serverShots, clientBoard, clientShots));
        }
        String winner = shots.get(shots.size() - 1).who;
        UserInterface.print(MessageFormat.format(FOOTER, winner));
    }

    private char[][] getEmptyBoard() {
        char[][] emptyBoard = new char[10][10];
        for (var i = 0; i < 10; i++) {
            for (var j = 0; j < 10; j++) {
                emptyBoard[i][j] = '~';
            }
        }
        return emptyBoard;
    }

    private char[][] fulfilBoard(char[][] bord, BoardDto shipsOnTheBoard) {
        List<Coordinate> ships = convertBoardDtoToCoordinates(shipsOnTheBoard);
        for (Coordinate ship : ships) {
            bord[ship.row][ship.column] = 'O';
        }
        return bord;
    }

    private List<Coordinate> convertBoardDtoToCoordinates(BoardDto shipsOnTheBoard) {
        List<Coordinate> coordinates = new ArrayList<>();
        List<String> ships = shipsOnTheBoard.getAsList();
        for (var ship : ships) {
            coordinates.add(Coordinate.from(ship));
        }
        return coordinates;
    }

    private String generateFrame(char[][] serverBoard, char[][] serverShots, char[][] clientBoard, char[][] clientShots) {
        return generateBoards(serverBoard, serverShots, SERVER_HEADER) +
                "\n" +
                generateBoards(clientBoard, clientShots, CLIENT_HEADER);
    }

    private String generateBoards(char[][] leftBoard, char[][] rightBoard, String header) {
        StringBuilder boards = new StringBuilder();
        boards.append(header);
        var rowHeader = 'A';
        for (int i = 0; i < 10; i++) {
            boards.append(rowHeader)
                    .append(" ")
                    .append(generateLine(i, leftBoard))
                    .append("  ")
                    .append(rowHeader)
                    .append(" ")
                    .append(generateLine(i, rightBoard))
                    .append("\n");
            rowHeader++;
        }
        return boards.toString();
    }

    private String generateLine(int i, char[][] serverBoard) {
        StringBuilder line = new StringBuilder();
        char[] row = serverBoard[i];
        for (var r : row) {
            line.append(r).append(" ");
        }
        return line.toString();
    }

    private void updateBoards(Shot shot, char[][] serverBoard, char[][] serverShots, char[][] clientBoard, char[][] clientShots) {
        if ("CLIENT".equals(shot.who)) {
            update(shot, serverBoard, clientShots);
        } else {
            update(shot, clientBoard, serverShots);
        }
    }

    private void update(Shot shot, char[][] board, char[][] shots) {
        var row = shot.value.charAt(0) - 65;
        var column = Integer.parseInt(shot.value.substring(1)) - 1;
        char value = switch (shot.result) {
            case "MISS" -> '*';
            case "HIT", "SINKING" -> 'X';
            default -> throw new IllegalStateException("Unexpected value");
        };
        board[row][column] = value;
        shots[row][column] = value;
    }
}
