package pl.uncleglass.battleship.modes.server;

import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.response.Response;

class BadRequestCommand extends Command {

    protected BadRequestCommand(Request request) {
        super(request);
    }

    @Override
    public Response execute(Game game) {
        return Response.badRequest();
    }
}
