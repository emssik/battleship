package pl.uncleglass.battleship.modes.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.uncleglass.battleship.dtos.ShotDto;
import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.modes.shared.JsonConverter;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.response.Response;

class ResultRequestCommand extends Command {
    private static final Logger logger = LogManager.getLogger(ResultRequestCommand.class);

    protected ResultRequestCommand(Request request) {
        super(request);
    }

    @Override
    public Response execute(Game game) {
        var body = request.getBody().toString();
        var result = JsonConverter.from(body, String.class);
        if (result.equals("MISS")) {
            game.setMiss();
        }
        if (result.equals("HIT")) {
            game.setHit();
        }
        if (result.equals("SINKING")) {
            game.setSinking();
            if (game.whetherIWon()) {
                logger.info("Server won the game");
            }
        }
        ShotDto actualShot = game.getActualShot();
        DataManager dataManager = new DataManager();
        dataManager.saveServerShot(actualShot, result, game.getGameId());
        return Response.result();
    }
}
