package pl.uncleglass.battleship.modes.server;

class GameInfo {
    private final long id;
    private final String opponentName;
    private final String date;

    public GameInfo(long id, String opponentName, String date) {
        this.id = id;
        this.opponentName = opponentName;
        this.date = date;
    }

    public long getId() {
        return id;
    }

    public String getOpponentName() {
        return opponentName;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "game {" +
                "id=" + id +
                ", client=[" + opponentName +
                "], date=[" + date +
                "]}";
    }
}
