package pl.uncleglass.battleship.modes.server;

import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.modes.shared.JsonConverter;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.response.Response;
import pl.uncleglass.battleship.modes.shared.response.Status;

class GameInvitationRequestCommand extends Command {

    protected GameInvitationRequestCommand(Request request) {
        super(request);
    }

    @Override
    public Response execute(Game game) {
        game.prepareGame();
        DataManager dataManager = new DataManager();
        String boardFilling = JsonConverter.to(game.getBoardFilling());
        dataManager.saveServerBoard(boardFilling, game.getGameId());
        return Response.gameInvitation(Status.OK, null);
    }
}
