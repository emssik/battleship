package pl.uncleglass.battleship.modes.server;

import pl.uncleglass.battleship.UserInterface;
import pl.uncleglass.battleship.modes.Mode;
import pl.uncleglass.battleship.modes.exit.ExitMode;

public class SerwerStarter implements Mode {
    private static final String MAIN_MENU = """
            Choose what you want to do:
            [1] - Start game
            [2] - Browse games
            [3] - Back
            [4] - Exit
            """;

    @Override
    public boolean run() {
        var run = true;
        while (run) {
            UserInterface.print(MAIN_MENU);
            var choice = UserInterface.read();
            var mode = ServerModeFactory.get(choice);
            if (mode instanceof ExitMode) {
                return false;
            }
            run = mode.run();
            if (mode instanceof BrowseMode && !run) {
                return false;
            }
        }
        return true;
    }
}
