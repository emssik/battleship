package pl.uncleglass.battleship.modes.server;

import pl.uncleglass.battleship.UserInterface;
import pl.uncleglass.battleship.dtos.BoardDto;
import pl.uncleglass.battleship.modes.Mode;
import pl.uncleglass.battleship.modes.shared.JsonConverter;

import java.util.List;

class DisplayMode implements Mode {
    private final DataManager dataManager = new DataManager();
    @Override
    public boolean run() {
        long gameId = chooseGame();
        CourseOfTheGame game = getGame(gameId);
        long speed = choseDisplaySpeed();
        game.display(speed);

        return true;
    }

    private long chooseGame() {
        UserInterface.print("Games:");
        List<GameInfo> gameInfoList = dataManager.getGamesInfo();
        for (GameInfo gameInfo : gameInfoList) {
            UserInterface.print(gameInfo.toString());
        }
        while (true) {
            String idInString = UserInterface.read("Enter id of game to display: ");
            try {
                long finalId = Long.parseLong(idInString);
                boolean match = gameInfoList.stream()
                        .map(GameInfo::getId)
                        .anyMatch(aLong -> aLong == finalId);
                if (!match) {
                    UserInterface.print("Id does not exist. Select existing one.");
                    continue;
                }
                return finalId;
            } catch (NumberFormatException e) {
                UserInterface.print("Id must be a number!");
            }
        }
    }

    private CourseOfTheGame getGame(long gameId) {
        CourseOfTheGame game = new CourseOfTheGame();
        String serverBoard = dataManager.getServerBoard(gameId);
        game.setServerShipsOnTheBoard(JsonConverter.from(serverBoard, BoardDto.class));
        String clientBoard = dataManager.getClientBoard(gameId);
        game.setClientShipsOnTheBoard(JsonConverter.from(clientBoard, BoardDto.class));
        game.setShots(dataManager.getShots(gameId));
        return game;
    }

    private long choseDisplaySpeed() {
        String menu = """
                Choose display speed:
                [1/a] - At once
                [2/f] - Fast (1 sec)
                [3/m] - Middle (6 sec)
                [4/s] - Slow (12 sec)
                """;
        long speed;
        while (true) {
            String read = UserInterface.read(menu);
            speed = switch (read) {
              case "1", "a" -> 0;
              case "2", "f" -> 1000;
              case "3", "m" -> 6000;
              case "4", "s" -> 12000;
                default -> -1;
            };
            if (speed != -1) {
                break;
            }
            UserInterface.print("Unknown command.");
        }
        return speed;
    }
}
