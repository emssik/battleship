package pl.uncleglass.battleship.modes.server;

import pl.uncleglass.battleship.game.Game;
import pl.uncleglass.battleship.modes.shared.request.Request;
import pl.uncleglass.battleship.modes.shared.response.Response;

abstract class Command {
    protected final Request request;

    protected Command(Request request) {
        this.request = request;
    }

    public abstract Response execute(Game game);
}
