package pl.uncleglass.battleship.modes.exit;

import pl.uncleglass.battleship.modes.Mode;

public class BackMode implements Mode {
    @Override
    public boolean run() {
        return false;
    }
}
