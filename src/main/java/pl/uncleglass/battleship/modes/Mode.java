package pl.uncleglass.battleship.modes;

public interface Mode {
    boolean run();
}
