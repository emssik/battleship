package pl.uncleglass.battleship.game;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class Fleet {
    private final Set<Ship> ships = new HashSet<>();

    private Fleet() {
    }

    public static Fleet create(Board board) {
        var fleet = new Fleet();
        Set<Ship> assembly = ShipAssembler.assembly(board.getShipsCoordinates());
        fleet.ships.addAll(assembly);
        return fleet;
    }

    public void removeSunkenShips(Board board) {
        Set<Ship> assembly = ShipAssembler.assembly(board.getHitsCoordinates());
        ships.removeAll(assembly);
    }

    public int size() {
        return ships.size();
    }

    public Set<Ship> getShips() {
        return Set.copyOf(ships);
    }

    public static List<Integer> getMissingShipsSizeList(Set<Ship> ships) {
        int[] shipsSizeArray = {0, 4, 3, 2, 1};
        ships.forEach(ship -> shipsSizeArray[ship.size()]--);
        List<Integer> list = new ArrayList<>();
        for (var shipSize = 1; shipSize < shipsSizeArray.length; shipSize++) {
            for (var i = 0; i < shipsSizeArray[shipSize]; i++) {
                list.add(shipSize);
            }
        }
        return list;
    }
}
