package pl.uncleglass.battleship.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

class AIProbabilityShotDecisionProvider implements ShotDecisionProvider {
    private final Random random = new Random();

    @Override
    public Coordinate nextShot(Shots shots, Board board) {
        List<Coordinate> lastContinuousHitList = shots.getLastContinuousHitList();
        if (lastContinuousHitList.isEmpty()) {
            return getNextShot(board);
        } else if (lastContinuousHitList.size() == 1) {
            return findShotByNeighbours(lastContinuousHitList.get(0), board);
        } else {
            return findShot(lastContinuousHitList, board);
        }
    }

    private Coordinate getNextShot(Board board) {
        List<Coordinate> coordinates = board.getHitsCoordinates();
        Set<Ship> fleet = ShipAssembler.assembly(coordinates);
        List<Integer> missingShipsSizeList = Fleet.getMissingShipsSizeList(fleet);
        var probabilityMatrix = new ProbabilityMatrix();
        missingShipsSizeList.forEach(shipSize -> {
            Set<Ship> generate = ShipGenerator.generate(shipSize, board);
            probabilityMatrix.count(generate);
        });
        List<Coordinate> coordinatesWithHighestProbability = probabilityMatrix.getCoordinatesWithHighestProbability();
        var index = random.nextInt(coordinatesWithHighestProbability.size());
        return coordinatesWithHighestProbability.get(index);
    }

    private Coordinate findShotByNeighbours(Coordinate coordinate, Board board) {
        List<Coordinate> readyForShotNeighbours = board.getReadyForShotNeighbours(coordinate);
        var index = random.nextInt(readyForShotNeighbours.size());
        return readyForShotNeighbours.get(index);
    }

    private Coordinate findShot(List<Coordinate> lastContinuousHitList, Board board) {
        sortHitList(lastContinuousHitList);
        Coordinate head = lastContinuousHitList.get(0);
        Coordinate tail = lastContinuousHitList.get(lastContinuousHitList.size() - 1);
        List<Coordinate> candidates = new ArrayList<>();
        if (isShipHorizontal(head, tail)) {
            addCandidatesByColumn(board, head, tail, candidates);
        } else {
            addCandidatesByRow(board, head, tail, candidates);
        }
        var index = random.nextInt(candidates.size());
        return candidates.get(index);
    }

    private void sortHitList(List<Coordinate> lastContinuousHitList) {
        lastContinuousHitList.sort((c1, c2) -> {
            if (isShipHorizontal(c1, c2)) {
                return Integer.compare(c1.getColumn(), c2.getColumn());
            } else {
                return Integer.compare(c1.getRow(), c2.getRow());
            }
        });
    }

    private boolean isShipHorizontal(Coordinate head, Coordinate tail) {
        return head.getRow() - tail.getRow() == 0;
    }

    private void addCandidatesByColumn(Board board, Coordinate head, Coordinate tail, List<Coordinate> candidates) {
        int c1 = head.getColumn() - 1;
        int c2 = tail.getColumn() + 1;
        if (Coordinate.isInTheRange(c1)) {
            var coordinate = Coordinate.of(head.getRow(), c1);
            if (board.isNotReserved(coordinate)) {
                candidates.add(coordinate);
            }
        }
        if (Coordinate.isInTheRange(c2)) {
            var coordinate = Coordinate.of(head.getRow(), c2);
            if (board.isNotReserved(coordinate)) {
                candidates.add(coordinate);
            }
        }
    }

    private void addCandidatesByRow(Board board, Coordinate head, Coordinate tail, List<Coordinate> candidates) {
        int r1 = head.getRow() - 1;
        int r2 = tail.getRow() + 1;
        if (Coordinate.isInTheRange(r1)) {
            var coordinate = Coordinate.of(r1, head.getColumn());
            if (board.isNotReserved(coordinate)) {
                candidates.add(coordinate);
            }
        }
        if (Coordinate.isInTheRange(r2)) {
            var coordinate = Coordinate.of(r2, head.getColumn());
            if (board.isNotReserved(coordinate)) {
                candidates.add(coordinate);
            }
        }
    }
}
