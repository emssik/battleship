package pl.uncleglass.battleship.game;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class Board {
    public static final int BOARD_SIZE = 10;
    private final Field[][] fields = new Field[BOARD_SIZE + 1][BOARD_SIZE + 1];
    private final Set<Coordinate> reserved = new HashSet<>();

    public Board() {
        for (var rowIndex = 1; rowIndex <= BOARD_SIZE; rowIndex++) {
            for (var columnIndex = 1; columnIndex <= BOARD_SIZE; columnIndex++) {
                fields[rowIndex][columnIndex] = new Field();
            }
        }
    }

    public void setShip(Ship ship) {
        Set<Coordinate> coordinates = ship.getCoordinates();
        if (verifyAddingPossibility(coordinates)) {
            coordinates.forEach(this::setShipField);
        } else {
            throw new IllegalArgumentException("You cannot add a ship at this location");
        }
    }

    public boolean verifyAddingPossibility(Set<Coordinate> coordinates) {
        return coordinates.stream()
                .noneMatch(this::isReserved);
    }

    private void setShipField(Coordinate coordinate) {
        getField(coordinate).ship();
        addReserved(coordinate);
    }

    public boolean isReserved(Coordinate coordinate) {
        return reserved.contains(coordinate);
    }

    private Field getField(Coordinate coordinate) {
        return fields[coordinate.getRow()][coordinate.getColumn()];
    }

    private void addReserved(Coordinate coordinate) {
        var row = coordinate.getRow();
        var column = coordinate.getColumn();
        for (var r = row - 1; r <= row + 1; r++) {
            for (var c = column - 1; c <= column + 1; c++) {
                if (Coordinate.isInTheRange(r) && Coordinate.isInTheRange(c)) {
                    reserved.add(Coordinate.of(r, c));
                }
            }
        }
    }

    public void setHit(Coordinate coordinate) {
        getField(coordinate).hit();
    }

    public void setMiss(Coordinate coordinate) {
        reserved.add(coordinate);
        getField(coordinate).miss();
    }

    public List<Coordinate> getShipsCoordinates() {
        List<Coordinate> coordinates = new LinkedList<>();
        for (var rowIndex = 1; rowIndex <= BOARD_SIZE; rowIndex++) {
            for (var columnIndex = 1; columnIndex <= BOARD_SIZE; columnIndex++) {
                var field = fields[rowIndex][columnIndex];
                if (field.isShip()) {
                    coordinates.add(Coordinate.of(rowIndex, columnIndex));
                }
            }
        }
        return coordinates;
    }

    public void updateReserved() {
        getHitsCoordinates()
                .forEach(this::addReserved);
    }

    public List<Coordinate> getHitsCoordinates() {
        List<Coordinate> coordinates = new LinkedList<>();
        for (var rowIndex = 1; rowIndex <= BOARD_SIZE; rowIndex++) {
            for (var columnIndex = 1; columnIndex <= BOARD_SIZE; columnIndex++) {
                var field = fields[rowIndex][columnIndex];
                if (field.isHit()) {
                    coordinates.add(Coordinate.of(rowIndex, columnIndex));
                }
            }
        }
        return coordinates;
    }

    public List<Coordinate> getReadyForShotNeighbours(Coordinate coordinate) {
        return getPossibleShotsCoordinates().stream()
                .filter(c -> c.isNeighbour(coordinate))
                .collect(Collectors.toList());
    }

    private List<Coordinate> getPossibleShotsCoordinates() {
        List<Coordinate> coordinates = new ArrayList<>();
        for (var rowIndex = 1; rowIndex <= BOARD_SIZE; rowIndex++) {
            for (var columnIndex = 1; columnIndex <= BOARD_SIZE; columnIndex++) {
                var field = fields[rowIndex][columnIndex];
                if (field.isEmpty()) {
                    var coordinate = Coordinate.of(rowIndex, columnIndex);
                    if (isNotReserved(coordinate)) {
                        coordinates.add(coordinate);
                    }
                }
            }
        }
        return coordinates;
    }

    public boolean isNotReserved(Coordinate coordinate) {
        return !isReserved(coordinate);
    }

    public boolean isFieldEmpty(Coordinate coordinate) {
        return getField(coordinate).isEmpty();
    }

    public boolean isFieldMiss(Coordinate coordinate) {
        return getField(coordinate).isMiss();
    }

    public boolean isFieldHit(Coordinate coordinate) {
        return getField(coordinate).isHit();
    }

    public boolean isFieldShip(Coordinate coordinate) {
        return getField(coordinate).isShip();
    }

    public int hitsCount() {
        return getHitsCoordinates().size();
    }

    int getReservedSize() {
        return reserved.size();
    }
}
