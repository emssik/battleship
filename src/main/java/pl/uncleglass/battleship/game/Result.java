package pl.uncleglass.battleship.game;

enum Result {
    MISS, HIT, SINKING
}
