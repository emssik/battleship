package pl.uncleglass.battleship.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class ProbabilityMatrix {
    private final int[][] fields = new int[Board.BOARD_SIZE + 1][Board.BOARD_SIZE + 1];

    public void count(Set<Ship> ships) {
        ships.forEach(ship -> {
            Set<Coordinate> coordinates = ship.getCoordinates();
            coordinates.forEach(
                    coordinate -> fields[coordinate.getRow()][coordinate.getColumn()]++
            );
        });
    }

    private static class ProbabilityField {
        private final int probability;
        private final Coordinate coordinate;

        private ProbabilityField(int probability, Coordinate coordinate) {
            this.probability = probability;
            this.coordinate = coordinate;
        }
    }

    public List<Coordinate> getCoordinatesWithHighestProbability() {
        List<ProbabilityField> list = new ArrayList<>();
        var highestProbability = 0;
        for (var row = 1; row <= Board.BOARD_SIZE; row++) {
            for (var column = 1; column <= Board.BOARD_SIZE; column++) {
                var probability = fields[row][column];
                if (probability > 0) {
                    list.add(new ProbabilityField(probability, Coordinate.of(row, column)));
                    if (highestProbability < probability) {
                        highestProbability = probability;
                    }
                }
            }
        }
        var finalHighestProbability = highestProbability;
        return list.stream()
                .filter(probabilityField -> probabilityField.probability == finalHighestProbability)
                .map(probabilityField -> probabilityField.coordinate)
                .collect(Collectors.toList());
    }
}
