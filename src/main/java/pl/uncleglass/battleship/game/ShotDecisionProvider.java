package pl.uncleglass.battleship.game;

interface ShotDecisionProvider {
    Coordinate nextShot(Shots shots, Board board);
}
