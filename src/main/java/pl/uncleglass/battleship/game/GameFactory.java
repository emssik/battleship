package pl.uncleglass.battleship.game;

public class GameFactory {
    private GameFactory() {
    }

    public static Game manualGame() {
        return new Game(new ManualShotDecisionProvider());
    }

    public static Game aiGame() {
        return new Game(new AIProbabilityShotDecisionProvider());
    }
}
