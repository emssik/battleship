package pl.uncleglass.battleship.game;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

class ShipGenerator {
    private ShipGenerator() {
    }

    public static Set<Ship> generate(int shipSize, Board board) {
        Set<Ship> ships = new HashSet<>();
        for (var row = 1; row <= Board.BOARD_SIZE; row++) {
            for (var column = 1; column <= Board.BOARD_SIZE - shipSize + 1; column++) {
                Set<Coordinate> coordinates = new HashSet<>();
                int finalR = row;
                IntStream.range(column, column + shipSize)
                        .forEach(value -> coordinates.add(Coordinate.of(finalR, value)));
                if (board.verifyAddingPossibility(coordinates)) {
                    ships.add(new Ship(coordinates));
                }
            }
        }
        for (var column = 1; column <= Board.BOARD_SIZE; column++) {
            for (var row = 1; row <= Board.BOARD_SIZE - shipSize + 1; row++) {
                Set<Coordinate> coordinates = new HashSet<>();
                int finalC = column;
                IntStream.range(row, row + shipSize)
                        .forEach(value -> coordinates.add(Coordinate.of(value, finalC)));
                if (board.verifyAddingPossibility(coordinates)) {
                    ships.add(new Ship(coordinates));
                }
            }
        }
        return ships;
    }
}
