package pl.uncleglass.battleship.game;

import pl.uncleglass.battleship.UserInterface;

class ManualShotDecisionProvider implements ShotDecisionProvider {
    @Override
    public Coordinate nextShot(Shots shots, Board board) {
        String read = UserInterface.read("Enter shot [ex. F2]");
        return Coordinate.from(read);
    }
}
