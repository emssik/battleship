package pl.uncleglass.battleship.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Shots {
    private final List<Shot> shots = new ArrayList<>();
    private Shot actual;

    public Coordinate getActualShot() {
        return actual.coordinate;
    }

    private class Shot {

        private Coordinate coordinate;
        private Result result;

        private Shot(Result result) {
            this.result = result;
        }

        private Shot(Coordinate coordinate) {
            this.coordinate = coordinate;
        }

        private boolean isHit() {
            return result == Result.HIT || result == Result.SINKING;
        }
    }

    public void setActual(Coordinate coordinate) {
        actual = new Shot(coordinate);
    }

    public Coordinate actual() {
        return actual.coordinate;
    }

    public void setHitAsResult() {
        actual.result = Result.HIT;
        shots.add(actual);
    }

    public void setMissAsResult() {
        actual.result = Result.MISS;
        shots.add(actual);
    }

    public void setSinkingAsResult() {
        actual.result = Result.SINKING;
        shots.add(actual);
    }

    public boolean isTheLastShotAHit() {
        Result result = lastShot().result;
        return result == Result.HIT;
    }

    public boolean isTheLastShotASinking() {
        Result result = lastShot().result;
        return result == Result.SINKING;
    }

    private Shot lastShot() {
        int index = lastShotIndex();
        if (index == -1) {
            return new Shot(Result.MISS);
        }
        return shots.get(index);
    }

    private int lastShotIndex() {
        return shots.size() - 1;
    }

    public List<Coordinate> getLastContinuousHitList() {
        List<Coordinate> list = new ArrayList<>();
        for (int i = lastShotIndex(); i >= 0; i--) {
            Shot shot = shots.get(i);
            if (shot.result == Result.HIT) {
                list.add(shot.coordinate);
            }
            if (shot.result == Result.SINKING) {
                break;
            }
        }
        return list;
    }
}
