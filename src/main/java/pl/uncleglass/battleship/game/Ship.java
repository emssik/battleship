package pl.uncleglass.battleship.game;

import java.util.Objects;
import java.util.Set;

class Ship {
    private final Set<Coordinate> coordinates;

    public Ship(Set<Coordinate> coordinates) {
        this.coordinates = coordinates;
    }

    public Set<Coordinate> getCoordinates() {
        return coordinates;
    }

    public int size() {
        return coordinates.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ship ship = (Ship) o;
        return Objects.equals(coordinates, ship.coordinates);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinates);
    }

    @Override
    public String toString() {
        return "Ship " +
                "coordinates= " + coordinates;
    }
}
