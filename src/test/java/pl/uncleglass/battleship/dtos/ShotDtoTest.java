package pl.uncleglass.battleship.dtos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class ShotDtoTest {

    @ParameterizedTest
    @MethodSource("lowercaseCharacters")
    @DisplayName("Should create ShotDto from lowercase character")
    void shouldCreateShotDtoFromLowercaseCharacter(char r, int c, String s) {
        ShotDto dto = ShotDto.of(r, c);

        assertThat(dto.toString()).hasToString(s);
    }

    public static Stream<Arguments> lowercaseCharacters() {
        return Stream.of(
                Arguments.of('a', 1, "A1"),
                Arguments.of('b', 2, "B2"),
                Arguments.of('c', 3, "C3"),
                Arguments.of('d', 4, "D4"),
                Arguments.of('e', 5, "E5"),
                Arguments.of('f', 6, "F6"),
                Arguments.of('g', 7, "G7"),
                Arguments.of('h', 8, "H8"),
                Arguments.of('i', 9, "I9"),
                Arguments.of('j', 10, "J10")
        );
    }

    @ParameterizedTest
    @MethodSource("uppercaseCharacters")
    @DisplayName("Should create ShotDto from uppercase character")
    void shouldCreateShotDtoFromUppercaseCharacter(char r, int c, String s) {
        ShotDto dto = ShotDto.of(r, c);

        assertThat(dto.toString()).hasToString(s);
    }

    public static Stream<Arguments> uppercaseCharacters() {
        return Stream.of(
                Arguments.of('A', 1, "A1"),
                Arguments.of('B', 2, "B2"),
                Arguments.of('C', 3, "C3"),
                Arguments.of('D', 4, "D4"),
                Arguments.of('E', 5, "E5"),
                Arguments.of('F', 6, "F6"),
                Arguments.of('G', 7, "G7"),
                Arguments.of('H', 8, "H8"),
                Arguments.of('I', 9, "I9"),
                Arguments.of('J', 10, "J10")
        );
    }

    @ParameterizedTest
    @MethodSource("toPass")
    @DisplayName("Should return true when verification succeed")
    void shouldReturnTrueWhenVerificationSucceed(char r, int c) {
        ShotDto dto = ShotDto.of(r, c);

        boolean verify = dto.verify();

        assertThat(verify).isTrue();
    }

    public static Stream<Arguments> toPass() {
        return Stream.of(
                Arguments.of('A', 1),
                Arguments.of('B', 2),
                Arguments.of('C', 3),
                Arguments.of('D', 4),
                Arguments.of('E', 5),
                Arguments.of('F', 6),
                Arguments.of('G', 7),
                Arguments.of('H', 8),
                Arguments.of('I', 9),
                Arguments.of('J', 10),
                Arguments.of('a', 1),
                Arguments.of('b', 2),
                Arguments.of('c', 3),
                Arguments.of('d', 4),
                Arguments.of('e', 5),
                Arguments.of('f', 6),
                Arguments.of('g', 7),
                Arguments.of('h', 8),
                Arguments.of('i', 9),
                Arguments.of('j', 10)
        );
    }

    @ParameterizedTest
    @MethodSource("toFail")
    @DisplayName("Should return false when verification failed")
    void shouldReturnFalseWhenVerificationFailed(char r, int c) {
        ShotDto dto = ShotDto.of(r, c);

        boolean verify = dto.verify();

        assertThat(verify).isFalse();
    }

    public static Stream<Arguments> toFail() {
        return Stream.of(
                Arguments.of('A', 0),
                Arguments.of('B', 11),
                Arguments.of('C', -3),
                Arguments.of('D', 44),
                Arguments.of('K', 5),
                Arguments.of('Z', 6),
                Arguments.of('@', 7),
                Arguments.of(']', 8)
        );
    }
}