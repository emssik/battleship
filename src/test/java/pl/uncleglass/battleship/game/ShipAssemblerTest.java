package pl.uncleglass.battleship.game;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class ShipAssemblerTest implements SampleBoard {

    @Test
    @DisplayName("Should assembly ships")
    void shouldAssemblyShips() {
        Board board = aBoardWithShips(
                aShip("A3","A4","A5","A6"),
                aShip("G7","G8","G9"),
                aShip("I2","I3","I4"),
                aShip("D7","E7"),
                aShip("F5","G5"),
                aShip("I6","J6"),
                aShip("C3"),
                aShip("D1"),
                aShip("A8"),
                aShip("G1")
        );

        Set<Ship> assembly = ShipAssembler.assembly(board.getShipsCoordinates());

        assertThat(assembly.size()).isEqualTo(10);
    }
}