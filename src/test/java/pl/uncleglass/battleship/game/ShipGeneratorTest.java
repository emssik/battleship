package pl.uncleglass.battleship.game;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class ShipGeneratorTest implements SampleShip {
    private Board board;

    @BeforeEach
    void setUp() {
        board = new Board();
    }

    @Test
    @DisplayName("Should generate 140 four-mast ships for empty board")
    void shouldGenerate140FourMastShipsForEmptyBoard() {
        Set<Ship> ships = ShipGenerator.generate(4, board);

        assertThat(ships.size()).isEqualTo(140);
    }

    @Test
    @DisplayName("Should generate 160 three-mast ships for empty board")
    void shouldGenerate160ThreeMastShipsForEmptyBoard() {
        Set<Ship> ships = ShipGenerator.generate(3, board);

        assertThat(ships.size()).isEqualTo(160);
    }

    @Test
    @DisplayName("Should generate 180 two-mast ships for empty board")
    void shouldGenerate180TwoMastShipsForEmptyBoard() {
        Set<Ship> ships = ShipGenerator.generate(2, board);

        assertThat(ships.size()).isEqualTo(180);
    }

    @Test
    @DisplayName("Should generate 100 single-mast ships for empty board")
    void shouldGenerate100SingleMastShipsForEmptyBoard() {
        Set<Ship> ships = ShipGenerator.generate(1, board);

        assertThat(ships.size()).isEqualTo(100);
    }

    @Test
    @DisplayName("Should generate 89 four-mast ships when is added three-mast ship in the middle of the board")
    void shouldGenerate89FourMastShipsWhenIsAddedThreeMastShipInTheMiddleOfTheBoard() {
        board.setShip(aShip("D5", "E5", "F5"));

        Set<Ship> ships = ShipGenerator.generate(4, board);

        assertThat(ships.size()).isEqualTo(89);
    }

    @Test
    @DisplayName("Should generate 81 three-mast ships when are added four-mast and three-mast ships in the middle of the board")
    void shouldGenerate81ThreeMastShipsWhenAreAddedFourMastAndThreeMastShipsInTheMiddleOfTheBoard() {
        board.setShip(aShip("C2", "D2", "E2", "F2"));
        board.setShip(aShip("D6", "D7", "D8"));

        Set<Ship> ships = ShipGenerator.generate(3, board);

        assertThat(ships.size()).isEqualTo(81);
    }

    @Test
    @DisplayName("Should generate 71 two-mast ships when are added four-mast and three-mast ships in the middle of the board")
    void shouldGenerate71TwoMastShipsWhenAreAddedFourMastAndThreeMastShipsInTheMiddleOfTheBoard() {
        board.setShip(aShip("C2", "D2", "E2", "F2"));
        board.setShip(aShip("D6", "D7", "D8"));
        board.setShip(aShip("G6", "H6", "I6"));

        Set<Ship> ships = ShipGenerator.generate(2, board);

        assertThat(ships.size()).isEqualTo(71);
    }
}