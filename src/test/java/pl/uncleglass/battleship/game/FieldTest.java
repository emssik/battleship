package pl.uncleglass.battleship.game;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class FieldTest {
    private Field field;

    @BeforeEach
    void setUp() {
        field = new Field();
    }

    @Test
    @DisplayName("New field should be empty")
    void newFieldShouldBeEmpty() {
        assertThat(field.isEmpty()).isTrue();
    }

    @Test
    @DisplayName("Should be a ship")
    void shouldBeAShip() {
        field.ship();

        assertThat(field.isShip()).isTrue();
    }

    @Test
    @DisplayName("Should be a hit")
    void shouldBeAHit() {
        field.hit();

        assertThat(field.isHit()).isTrue();
    }

    @Test
    @DisplayName("Should be a miss")
    void shouldBeAMiss() {
        field.miss();

        assertThat(field.isMiss()).isTrue();
    }
}