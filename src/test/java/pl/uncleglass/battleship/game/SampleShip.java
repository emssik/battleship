package pl.uncleglass.battleship.game;

import java.util.stream.Collectors;

import static java.util.Arrays.stream;

interface SampleShip {

    default Ship aShip(String... coordinates) {
        return new Ship(stream(coordinates)
                .map(Coordinate::from)
                .collect(Collectors.toSet())
        );
    }
}
