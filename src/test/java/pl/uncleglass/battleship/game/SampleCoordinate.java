package pl.uncleglass.battleship.game;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

interface SampleCoordinate {

    default Coordinate aCoordinate(String coordinate) {
        return Coordinate.from(coordinate);
    }

    default List<Coordinate> aCoordinateList(String... coordinates) {
        return stream(coordinates)
                .map(this::aCoordinate)
                .collect(Collectors.toList());
    }

    default Set<Coordinate> aCoordinateSet(String... coordinates) {
        return Set.copyOf(aCoordinateList(coordinates));
    }
}
