package pl.uncleglass.battleship.game;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class FleetTest implements SampleBoard, SampleCoordinate {

    @Test
    @DisplayName("Should create fleet")
    void shouldCreateFleet() {
        Fleet fleet = Fleet.create(getBoard());

        assertThat(fleet.size()).isEqualTo(10);
    }

    @Test
    @DisplayName("Should remove sunken ships from fleet")
    void shouldRemoveSunkenShipsFromFleet() {
        Board board = getBoard();
        Fleet fleet = Fleet.create(getBoard());
        board.setHit(aCoordinate("C3"));
        board.setHit(aCoordinate("I6"));
        board.setHit(aCoordinate("J6"));
        board.setHit(aCoordinate("D7"));
        board.setHit(aCoordinate("A3"));
        board.setHit(aCoordinate("A4"));

        fleet.removeSunkenShips(board);

        assertThat(fleet.size()).isEqualTo(8);
    }

    private Board getBoard() {
        return aBoardWithShips(
                aShip("A3","A4","A5","A6"),
                aShip("G7","G8","G9"),
                aShip("I2","I3","I4"),
                aShip("D7","E7"),
                aShip("F5","G5"),
                aShip("I6","J6"),
                aShip("C3"),
                aShip("D1"),
                aShip("A8"),
                aShip("G1")
        );
    }

    @Test
    @DisplayName("Should return ten-element list for empty set")
    void shouldReturnTenElementListForEmptySet() {
        List<Integer> missingShipsSizeList = Fleet.getMissingShipsSizeList(Collections.emptySet());

        assertThat(missingShipsSizeList.size()).isEqualTo(10);
    }

    @Test
    @DisplayName("List should contain four elements with value of one for empty set")
    void listShouldContainFourElementsWithValueOfOneForEmptySet() {
        List<Integer> missingShipsSizeList = Fleet.getMissingShipsSizeList(Collections.emptySet());

        List<Integer> integers = missingShipsSizeList.stream()
                .filter(integer -> integer.equals(1))
                .collect(Collectors.toList());
        assertThat(integers.size()).isEqualTo(4);
    }

    @Test
    @DisplayName("List should contain three elements with value of one for set contains only one-mast ship")
    void listShouldContainThreeElementsWithValueOfOneForSetContainsOnlyOneMastShip() {
        List<Integer> missingShipsSizeList = Fleet.getMissingShipsSizeList(Set.of(aShip("D4")));

        List<Integer> integers = missingShipsSizeList.stream()
                .filter(integer -> integer.equals(1))
                .collect(Collectors.toList());
        assertThat(integers.size()).isEqualTo(3);
    }
}