package pl.uncleglass.battleship.game;

import static java.util.Arrays.stream;

interface SampleBoard extends SampleShip {

    default Board emptyBoard() {
        return new Board();
    }

    default Board aBoardWithShips() {
        return aBoardWithShips(aShip("D3", "E3", "F3"),
                aShip("H3"),
                aShip("I6", "I7", "I8"),
                aShip("F9", "G9"),
                aShip("D7")
        );
    }

    default Board aBoardWithShips(Ship... ships) {
        Board board = new Board();
        stream(ships).forEach(board::setShip);
        return board;
    }

    default Board aBoardWithShip(String... coordinates) {
        Board board = new Board();
        board.setShip(aShip(coordinates));
        return board;
    }

    default Board aBoardWithMiss(String... coordinates) {
        Board board = new Board();
        stream(coordinates)
                .map(Coordinate::from)
                .forEach(board::setMiss);
        return board;
    }

    default Board aBoardWithHit(String... coordinates) {
        Board board = new Board();
        stream(coordinates)
                .map(Coordinate::from)
                .forEach(board::setHit);
        return board;
    }
}
