package pl.uncleglass.battleship.game;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class BoardTest implements SampleShip, SampleCoordinate {
    private Board board;

    @BeforeEach
    void setUp() {
        board = new Board();
    }

    @Test
    @DisplayName("Should set a single-mast ship")
    void shouldSetASingleMastShip() {
        board.setShip(aShip("D4"));

        assertThat(board.isFieldShip(aCoordinate("D4"))).isTrue();
    }

    @ParameterizedTest
    @MethodSource("shipCoordinates")
    @DisplayName("Should set a four-mast ship")
    void shouldSetAFourMastShip(String coordinate) {
        board.setShip(aShip("D4", "D5", "D6", "D7"));

        assertThat(board.isFieldShip(aCoordinate(coordinate))).isTrue();
    }

    static Stream<Arguments> shipCoordinates() {
        return Stream.of(
                Arguments.of("D4"),
                Arguments.of("D5"),
                Arguments.of("D6"),
                Arguments.of("D7")
        );
    }

    @ParameterizedTest
    @MethodSource("coordinates")
    @DisplayName("Should throw an exception when trying set a ship in the wrong place")
    void shouldThrowAnExceptionWhenTryingSetAShipInTheWrongPlace(String coordinate) {
        board.setShip(aShip("D4"));

        Throwable thrown = catchThrowable(() -> board.setShip(aShip(coordinate)));

        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("You cannot add a ship at this location");
    }

    static Stream<Arguments> coordinates() {
        return Stream.of(
                Arguments.of("D3"),
                Arguments.of("D5"),
                Arguments.of("D4"),
                Arguments.of("C3"),
                Arguments.of("C5"),
                Arguments.of("C4"),
                Arguments.of("E3"),
                Arguments.of("E5"),
                Arguments.of("E4")
        );
    }

    @Test
    @DisplayName("Should not throw an exception when trying set ship in the correct place")
    void shouldNotThrowAnExceptionWhenTryingSetShipInTheCorrectPlace() {
        board.setShip(aShip("D4"));

        assertThatNoException().isThrownBy(() -> board.setShip(aShip("G7")));
    }

    @Test
    @DisplayName("Should return true when adding coordinate is possible")
    void shouldReturnTrueWhenAddingCoordinateIsPossible() {
        board.setShip(aShip("D4"));

        boolean possibility = board.verifyAddingPossibility(aCoordinateSet("G7"));

        assertThat(possibility).isTrue();
    }

    @Test
    @DisplayName("Should return true when adding coordinates is possible")
    void shouldReturnTrueWhenAddingCoordinatesIsPossible() {
        board.setShip(aShip("D4"));

        boolean possibility = board.verifyAddingPossibility(aCoordinateSet("G7", "G8", "G9"));

        assertThat(possibility).isTrue();
    }

    @Test
    @DisplayName("Should return false when adding coordinate is not possible")
    void shouldReturnFalseWhenAddingCoordinateIsNotPossible() {
        board.setShip(aShip("D4"));

        boolean possibility = board.verifyAddingPossibility(aCoordinateSet("E5"));

        assertThat(possibility).isFalse();
    }

    @Test
    @DisplayName("Should return false when adding at least one coordinate is not possible")
    void shouldReturnFalseWhenAddingAtLeastOneCoordinateIsNotPossible() {
        board.setShip(aShip("F6"));

        boolean possibility = board.verifyAddingPossibility(aCoordinateSet("G7", "G8", "G9"));

        assertThat(possibility).isFalse();
    }

    @Test
    @DisplayName("Should return true when field is reserved")
    void shouldReturnTrueWhenFieldIsReserved() {
        board.setShip(aShip("D4"));

        boolean reserved = board.isReserved(aCoordinate("D5"));

        assertThat(reserved).isTrue();

    }

    @Test
    @DisplayName("Should return false when field is not reserved")
    void shouldReturnFalseWhenFieldIsNotReserved() {
        boolean reserved = board.isReserved(aCoordinate("D4"));

        assertThat(reserved).isFalse();
    }

    @Test
    @DisplayName("Should set field as a hit")
    void shouldSetFieldAsAHit() {
        board.setHit(aCoordinate("D4"));

        assertThat(board.isFieldHit(aCoordinate("D4"))).isTrue();
    }

    @Test
    @DisplayName("Should set filed as a miss")
    void shouldSetFiledAsAMiss() {
        board.setMiss(aCoordinate("D4"));

        assertThat(board.isFieldMiss(aCoordinate("D4"))).isTrue();
    }

    @Test
    @DisplayName("Filed set as a miss should be reserved")
    void filedSetAsAMissShouldBeReserved() {
        board.setMiss(aCoordinate("D4"));

        assertThat(board.isReserved(aCoordinate("D4"))).isTrue();
    }

    @Test
    @DisplayName("Should return empty list for empty board")
    void shouldReturnEmptyListForEmptyBoard() {
        List<Coordinate> coordinateList = board.getShipsCoordinates();

        assertThat(coordinateList.size()).isZero();
    }

    @Test
    @DisplayName("Should return one-element list for board contains only one-mast ship")
    void shouldReturnOneElementListForBoardContainsOnlyOneMastShip() {
        board.setShip(aShip("D4"));

        List<Coordinate> coordinateList = board.getShipsCoordinates();

        assertThat(coordinateList.size()).isOne();
    }

    @Test
    @DisplayName("Should return four-element list for board contains only four-mast ship")
    void shouldReturnFourElementListForBoardContainsOnlyFourMastShip() {
        board.setShip(aShip("D4", "D5", "D6", "D7"));

        List<Coordinate> coordinateList = board.getShipsCoordinates();

        assertThat(coordinateList.size()).isEqualTo(4);
    }

    @Test
    @DisplayName("Should return four-element list for board contains four one-mast ships")
    void shouldReturnFourElementListForBoardContainsFourOneMastShips() {
        board.setShip(aShip("D4"));
        board.setShip(aShip("A1"));
        board.setShip(aShip("G7"));
        board.setShip(aShip("J10"));

        List<Coordinate> coordinateList = board.getShipsCoordinates();

        assertThat(coordinateList.size()).isEqualTo(4);
    }

    @ParameterizedTest
    @MethodSource("coordinates")
    @DisplayName("Should update reserved fields")
    void shouldUpdateReservedFields(String coordinate) {
        board.setHit(aCoordinate("D4"));

        board.updateReserved();

        assertThat(board.isReserved(aCoordinate(coordinate))).isTrue();
    }

    @Test
    @DisplayName("Should return empty list of hit coordinates for empty board")
    void shouldReturnEmptyListOfHitCoordinatesForEmptyBoard() {
        List<Coordinate> coordinateList = board.getHitsCoordinates();

        assertThat(coordinateList.size()).isZero();
    }

    @Test
    @DisplayName("Should return one-element list of hit coordinates for board contains one hit")
    void shouldReturnOneElementListOfHitCoordinatesForBoardContainsOneHit() {
        board.setHit(aCoordinate("D4"));

        List<Coordinate> coordinateList = board.getHitsCoordinates();

        assertThat(coordinateList.size()).isOne();
    }

    @Test
    @DisplayName("Should return four-element list of hit coordinates for board contains four hits")
    void shouldReturnFourElementListOfHitCoordinatesForBoardContainsFourHits() {
        board.setHit(aCoordinate("D4"));
        board.setHit(aCoordinate("D5"));
        board.setHit(aCoordinate("D6"));
        board.setHit(aCoordinate("J9"));

        List<Coordinate> coordinateList = board.getHitsCoordinates();

        assertThat(coordinateList.size()).isEqualTo(4);
    }

    @Test
    @DisplayName("Should return four-element list of ready for shot neighbours for a single field")
    void shouldReturnFourElementListOfReadyForShotNeighboursForASingleField() {
        board.setHit(aCoordinate("D4"));

        List<Coordinate> neighbours = board.getReadyForShotNeighbours(aCoordinate("D4"));

        assertThat(neighbours.size()).isEqualTo(4);
    }

    @Test
    @DisplayName("Should return three-element list of ready for shot neighbours for two adjacent fields")
    void shouldReturnThreeElementListOfReadyForShotNeighboursForTwoAdjacentFields() {
        board.setHit(aCoordinate("D4"));
        board.setHit(aCoordinate("D5"));

        List<Coordinate> neighbours = board.getReadyForShotNeighbours(aCoordinate("D4"));

        assertThat(neighbours.size()).isEqualTo(3);
    }

    @Test
    @DisplayName("Should set 9 reserved fields when single-mast ship is added to the board")
    void shouldSet9ReservedFieldsWhenSingleMastShipIsAddedToTheBoard() {
        board.setShip(aShip("D4"));

        int reservedSize = board.getReservedSize();

        assertThat(reservedSize).isEqualTo(9);
    }

    @Test
    @DisplayName("Should set 6 reserved fields when single-mast ship is added at the left edge of the board")
    void shouldSet6ReservedFieldsWhenSingleMastShipIsAddedAtTheLeftEdgeOfTheBoard() {
        board.setShip(aShip("D1"));

        int reservedSize = board.getReservedSize();

        assertThat(reservedSize).isEqualTo(6);
    }

    @Test
    @DisplayName("Should set 6 reserved fields when single-mast ship is added at the right edge of the board")
    void shouldSet6ReservedFieldsWhenSingleMastShipIsAddedAtTheRightEdgeOfTheBoard() {
        board.setShip(aShip("D10"));

        int reservedSize = board.getReservedSize();

        assertThat(reservedSize).isEqualTo(6);
    }

    @Test
    @DisplayName("Should set 6 reserved fields when single-mast ship is added at the top edge of the board")
    void shouldSet6ReservedFieldsWhenSingleMastShipIsAddedAtTheTopEdgeOfTheBoard() {
        board.setShip(aShip("A4"));

        int reservedSize = board.getReservedSize();

        assertThat(reservedSize).isEqualTo(6);
    }

    @Test
    @DisplayName("Should set 6 reserved fields when single-mast ship is added at the bottom edge of the board")
    void shouldSet6ReservedFieldsWhenSingleMastShipIsAddedAtTheBottomEdgeOfTheBoard() {
        board.setShip(aShip("J4"));

        int reservedSize = board.getReservedSize();

        assertThat(reservedSize).isEqualTo(6);
    }

    @Test
    @DisplayName("Should set 4 reserved fields when single-mast ship is added to the top left corner of the board")
    void shouldSet4ReservedFieldsWhenSingleMastShipIsAddedToTheTopLeftCornerOfTheBoard() {
        board.setShip(aShip("A1"));

        int reservedSize = board.getReservedSize();

        assertThat(reservedSize).isEqualTo(4);
    }

    @Test
    @DisplayName("Should set 4 reserved fields when single-mast ship is added to the top right corner of the board")
    void shouldSet4ReservedFieldsWhenSingleMastShipIsAddedToTheTopRightCornerOfTheBoard() {
        board.setShip(aShip("A10"));

        int reservedSize = board.getReservedSize();

        assertThat(reservedSize).isEqualTo(4);
    }

    @Test
    @DisplayName("Should set 4 reserved fields when single-mast ship is added to the bottom left corner of the board")
    void shouldSet4ReservedFieldsWhenSingleMastShipIsAddedToTheBottomLeftCornerOfTheBoard() {
        board.setShip(aShip("J1"));

        int reservedSize = board.getReservedSize();

        assertThat(reservedSize).isEqualTo(4);
    }

    @Test
    @DisplayName("Should set 4 reserved fields when single-mast ship is added to the bottom right corner of the board")
    void shouldSet4ReservedFieldsWhenSingleMastShipIsAddedToTheBottomRightCornerOfTheBoard() {
        board.setShip(aShip("A10"));

        int reservedSize = board.getReservedSize();

        assertThat(reservedSize).isEqualTo(4);
    }

    @Test
    @DisplayName("Should set 15 reserved fields when three-mast ship is added to the board")
    void shouldSet15ReservedFieldsWhenThreeMastShipIsAddedToTheBoard() {
        board.setShip(aShip("D5", "E5", "F5"));

        int reservedSize = board.getReservedSize();

        assertThat(reservedSize).isEqualTo(15);
    }

    @Test
    @DisplayName("Should throw an exception when you add a ship in the wrong place")
    void shouldThrowAnExceptionWhenYouAddAShipInTheWrongPlace() {
        board.setShip(aShip("D5", "E5", "F5"));

        assertThatIllegalArgumentException().isThrownBy(() ->
                        board.setShip(aShip("G6")))
                .withMessage("You cannot add a ship at this location");
    }

    @Test
    @DisplayName("Should throw an exception when you add a ship in the same place")
    void shouldThrowAnExceptionWhenYouAddAShipInTheSamePlace() {
        board.setShip(aShip("D5", "E5", "F5"));

        assertThatIllegalArgumentException().isThrownBy(() ->
                        board.setShip(aShip("E5")))
                .withMessage("You cannot add a ship at this location");
    }
}