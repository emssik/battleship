package pl.uncleglass.battleship.game;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AIProbabilityShotDecisionProviderTest implements SampleBoard, SampleCoordinate {
    private AIProbabilityShotDecisionProvider provider;
    private final Shots shots = mock(Shots.class);

    @BeforeEach
    void setUp() {
        provider = new AIProbabilityShotDecisionProvider();
    }

    @Test
    @DisplayName("Should return a specific coordinate")
    void shouldReturnASpecificCoordinate() {
        when(shots.getLastContinuousHitList()).thenReturn(Collections.emptyList());

        Coordinate coordinate = provider.nextShot(shots, aBoardWithShips());

        assertThat(coordinate).isEqualTo(aCoordinate("B5"));
    }

    @Test
    @DisplayName("Should return a specific neighbour coordinate")
    void shouldReturnASpecificNeighbourCoordinate() {
        when(shots.getLastContinuousHitList())
                .thenReturn(Collections.singletonList(aCoordinate("J4")));

        Coordinate coordinate = provider.nextShot(shots, aBoardWithShips());

        assertThat(coordinate).isEqualTo(aCoordinate("J3"));
    }

    @Test
    @DisplayName("Should return the specific coordinates of the neighbours")
    void shouldReturnTheSpecificCoordinatesOfTheNeighbours() {
        when(shots.getLastContinuousHitList())
                .thenReturn(Collections.singletonList(aCoordinate("B2")));

        Coordinate coordinate = provider.nextShot(shots, aBoardWithShips());

        assertThat(coordinate).isIn(aCoordinateList("B1", "B3", "A2"));
    }

    @Test
    @DisplayName("Should return a specific coordinate base on the sequence")
    void shouldReturnASpecificCoordinateBaseOnTheSequence() {
        when(shots.getLastContinuousHitList()).thenReturn(aCoordinateList("J3", "J4"));

        Coordinate coordinate = provider.nextShot(shots, aBoardWithShips());

        assertThat(coordinate).isEqualTo(aCoordinate("J2"));
    }

    @Test
    @DisplayName("Should return a specific coordinates base on the horizontal sequence")
    void shouldReturnASpecificCoordinatesBaseOnTheHorizontalSequence() {
        when(shots.getLastContinuousHitList()).thenReturn(aCoordinateList("B5", "B4"));

        Coordinate coordinate = provider.nextShot(shots, aBoardWithShips());

        assertThat(coordinate).isIn(aCoordinateList("B6", "B3"));
    }

    @Test
    @DisplayName("Should return a specific coordinates base on the vertical sequence")
    void shouldReturnASpecificCoordinatesBaseOnTheVerticalSequence() {
        when(shots.getLastContinuousHitList()).thenReturn(aCoordinateList("B5", "C5"));

        Coordinate coordinate = provider.nextShot(shots, aBoardWithShips());

        assertThat(coordinate).isIn(aCoordinateList("A5", "D5"));
    }
}